//
//  PlayerStatsService.swift
//  NRLStats
//
//  Created by Nicolas Parimeros on 21/08/2019.
//  Copyright © 2019 nicolas.parimeros. All rights reserved.
//

import Foundation

// Service used to fetch player stats from the API

protocol PlayerStatsServiceProtocol {
    func fetchPlayerStats(teamId: Int, playerId: Int, completion: @escaping (Bool, PlayerStats?) -> Void)
}

class PlayerStatsService: APIService, PlayerStatsServiceProtocol {

    func fetchPlayerStats(teamId: Int, playerId: Int, completion: @escaping (Bool, PlayerStats?) -> Void) {
        super.fetchFromApi(url: Router.Route.playerStats(teamId: teamId, playerId: playerId).url, completion: completion)
    }

}
