//
//  Router.swift
//  NRLStats
//
//  Created by Nicolas Parimeros on 21/08/2019.
//  Copyright © 2019 nicolas.parimeros. All rights reserved.
//

import Foundation

// This struct is used to handle all routing for the API

struct Router {

    enum Route {

        case stats
        case playerStats(teamId: Int, playerId: Int)

        var url: URL {
            var urlString: String

            switch self {
            case .stats: urlString = self.urlString
            case .playerStats(let teamId, let playerId): urlString = String(format: self.urlString, teamId, playerId)
            }

            guard let url = URL(string: urlString) else {
                fatalError("Undefined url for route \(self)")
            }

            return url
        }

        private var urlString: String {
            switch self {
            case .stats: return AppConstants.APIRessources.statsApiUrl
            case .playerStats: return AppConstants.APIRessources.playerStatsApiUrlFormat
            }
        }
    }
}
