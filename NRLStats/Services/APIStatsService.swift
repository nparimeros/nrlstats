//
//  APIStatsService.swift
//  NRLStats
//
//  Created by Nicolas Parimeros on 20/08/2019.
//  Copyright © 2019 nicolas.parimeros. All rights reserved.
//

import Foundation

// Service used to fetch stats categories from the API

protocol APIStatsServiceProtocol {
    func fetchStatCategories(completion: @escaping (_ success: Bool, _ stats: [StatCategory]?) -> Void)
}

class APIStatsService: APIService, APIStatsServiceProtocol {

    func fetchStatCategories(completion: @escaping (_ success: Bool, _ stats: [StatCategory]?) -> Void) {
        super.fetchFromApi(url: Router.Route.stats.url, completion: completion)
    }

}
