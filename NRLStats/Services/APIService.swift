//
//  APIService.swift
//  NRLStats
//
//  Created by Nicolas Parimeros on 21/08/2019.
//  Copyright © 2019 nicolas.parimeros. All rights reserved.
//

import Foundation

class APIService {

    // This method use the native URL session to fetch some data from an url and return a completion block
    // containing a success indicator and parsed result using JSONDecoder

    func fetchFromApi<T>(url: URL, completion: @escaping (_ success: Bool, _ result: T?) -> Void) where T: Decodable {
        URLSession.shared.dataTask(with: url) { data, response, error in
            DispatchQueue.main.async {
                guard let data = data else { return completion(false, nil) }

                do {
                    let res = try JSONDecoder().decode(T.self, from: data)
                    return completion(true, res)
                } catch {
                    return completion(false, nil)
                }

            }
        }.resume()
    }

}
