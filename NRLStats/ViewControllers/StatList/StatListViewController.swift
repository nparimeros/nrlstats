//
//  StatListViewController.swift
//  NRLStats
//
//  Created by Nicolas Parimeros on 20/08/2019.
//  Copyright © 2019 nicolas.parimeros. All rights reserved.
//

import UIKit
import SnapKit

class StatListViewController: UIViewController {

    // MARK: Constants

    private let kCellIdentifier = "category-cell"
    private let kRowHeight: CGFloat = 52

    // MARK: UI Components

    private lazy var rootView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()

    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.delegate = self
        tableView.dataSource = self
        return tableView
    }()

    private lazy var activityIndicator = UIActivityIndicatorView(style: .gray)

    // MARK: View model

    var viewModel: StatListViewModelProtocol!

    // MARK: Object lifecycle

    init() {
        super.init(nibName: nil, bundle: nil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Setup UI
    
    override func loadView() {
        self.view = rootView
        rootView.addSubview(tableView)
        rootView.addSubview(activityIndicator)
        buildConstraints()
    }
    
    private func buildConstraints() {
        tableView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        activityIndicator.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
    }

    // Dependency injection

    private func setup() {
        let viewController = self
        let viewModel = StatListViewModel()
        let apiStatsService = APIStatsService()
        viewModel.apiService = apiStatsService
        viewController.viewModel = viewModel
    }

    // MARK: View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        title = AppConstants.General.topPlayerStats

        // Set viewModel updateLoadingStatus block to get notified when to update the loading UI
        
        viewModel.updateLoadingStatus = { [weak self] in
            DispatchQueue.main.async {
                let isLoading = self?.viewModel.isLoading ?? false
                self?.tableView.isHidden = isLoading
                if isLoading {
                    self?.activityIndicator.startAnimating()
                } else {
                    self?.activityIndicator.stopAnimating()
                }
            }
        }

        // Set viewModel reloadTableView block to get notified when to reload the tableView
        
        viewModel.reloadTableView = { [weak self] in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }

        // Ask the viewModel to fetch the different stats categories
        
        viewModel.fetchStatCategories()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }

}

extension StatListViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.title(for: section)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return kRowHeight
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Navigate to StatCategory screen
        let statCategoryViewModel = viewModel.statCategoryViewModel(for: indexPath)
        let vc = StatCategoryViewController(viewModel: statCategoryViewModel)
        navigationController?.pushViewController(vc, animated: true)
    }

}

extension StatListViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfCells
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier) ?? UITableViewCell(style: .default, reuseIdentifier: kCellIdentifier)
        let model = viewModel.cellViewModel(for: indexPath)
        cell.textLabel?.text = model.title
        cell.accessoryType = .disclosureIndicator
        return cell
    }

}
