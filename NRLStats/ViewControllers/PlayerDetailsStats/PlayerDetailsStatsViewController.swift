//
//  PlayerDetailsStatsViewController.swift
//  NRLStats
//
//  Created by Nicolas Parimeros on 21/08/2019.
//  Copyright © 2019 nicolas.parimeros. All rights reserved.
//

import UIKit

class PlayerDetailsStatsViewController: UIViewController {

    // MARK: Constants

    private let kHeadshotHeight: CGFloat = 200
    private let kCellIdentifier = "stat-cell"

    // MARK: UI Components

    private lazy var headerImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()

    private lazy var rootView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()

    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.dataSource = self
        return tableView
    }()

    // MARK: View model

    var viewModel: PlayerDetailsStatsViewModelProtocol

    // MARK: Object lifecycle

    init(viewModel: PlayerDetailsStatsViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: Setup UI

    override func loadView() {
        self.view = rootView
        rootView.addSubview(headerImageView)
        rootView.addSubview(tableView)
        buildConstraints()
    }

    private func buildConstraints() {
        headerImageView.snp.makeConstraints { (make) in
            make.height.equalTo(kHeadshotHeight)
            make.top.equalTo(view.safeAreaLayoutGuide)
            make.left.right.equalToSuperview()
        }
        tableView.snp.makeConstraints { (make) in
            make.top.equalTo(headerImageView.snp.bottom)
            make.left.right.bottom.equalToSuperview()
        }
    }

    // MARK: View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup UI
        setupUI()

        // Set viewModel reloadTableView block to get notified when to reload the tableView
        viewModel.reloadTableView = { [weak self] in
            self?.tableView.reloadData()
        }

        // Ask the viewModel to fetch the player stats
        viewModel.fetchPlayerStats()
    }

    private func setupUI() {
        title = viewModel.title
        headerImageView.kf.setImage(with: viewModel.headerImageUrl)
    }

}

extension PlayerDetailsStatsViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.title(for: section)
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows(in: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier) ?? UITableViewCell(style: .value1, reuseIdentifier: kCellIdentifier)
        let model = viewModel.cellViewModel(for: indexPath)
        cell.textLabel?.text = model.title
        cell.detailTextLabel?.text = model.detail
        return cell
    }

}
