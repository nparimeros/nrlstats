//
//  StatCategoryViewController.swift
//  NRLStats
//
//  Created by Nicolas Parimeros on 20/08/2019.
//  Copyright © 2019 nicolas.parimeros. All rights reserved.
//

import UIKit
import SnapKit
import Kingfisher

class StatCategoryViewController: UIViewController {

    // MARK: Constants

    private let kCellIdentifier = "stat-cell"
    private let kMargin: CGFloat = 20
    private let kCellHeight: CGFloat = 200

    // MARK: UI Components

    private lazy var rootView = UIView()

    private lazy var headerView = StatCategoryHeaderView()

    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = kMargin
        layout.minimumInteritemSpacing = kMargin
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(PlayerStatCollectionViewCell.self, forCellWithReuseIdentifier: kCellIdentifier)
        collectionView.alwaysBounceVertical = true
        collectionView.backgroundColor = .white
        return collectionView
    }()

    // MARK: View model

    var viewModel: StatCategoryViewModelProtocol!

    // MARK: Object lifecycle

    init() {
        super.init(nibName: nil, bundle: nil)
    }

    init(viewModel: StatCategoryViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: Setup UI

    override func loadView() {
        edgesForExtendedLayout = []
        self.view = rootView
        rootView.addSubview(headerView)
        rootView.addSubview(collectionView)
        buildConstraints()
    }

    private func buildConstraints() {
        headerView.snp.makeConstraints { (make) in
            make.top.left.right.equalToSuperview()
        }
        collectionView.snp.makeConstraints { (make) in
            make.top.equalTo(headerView.snp.bottom)
            make.left.right.bottom.equalToSuperview()
        }
    }

    // MARK: View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        title = viewModel.title

        // Reload block
        viewModel.reloadCollectionView = { [weak self] in
            self?.reloadHeaderView()
            self?.collectionView.reloadData()
        }

        // Ask viewModel to fetch data
        viewModel.fetchTeamStats()
    }

    private func reloadHeaderView() {
        headerView.leftLabel.text = viewModel.leftHeaderText
        headerView.rightLabel.text = viewModel.rightHeaderText
    }

}

extension StatCategoryViewController: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // MARK: Navigate to player stats screen
        let playerStatsViewModel = viewModel.playerStatsViewModel(for: indexPath)
        let vc = PlayerDetailsStatsViewController(viewModel: playerStatsViewModel)
        navigationController?.pushViewController(vc, animated: true)
    }

}

extension StatCategoryViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: kMargin, left: kMargin, bottom: 0, right: kMargin)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewWidth = collectionView.frame.size.width - kMargin * 3
        return CGSize(width: collectionViewWidth / 2, height: kCellHeight)
    }

}

extension StatCategoryViewController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kCellIdentifier, for: indexPath) as! PlayerStatCollectionViewCell
        let model = viewModel.cellViewModel(for: indexPath)
        cell.configure(viewModel: model)
        return cell
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return viewModel.numberOfSections
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfItemsPerSection
    }

}
