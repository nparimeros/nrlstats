//
//  StatCategoryHeaderView.swift
//  NRLStats
//
//  Created by Nicolas Parimeros on 21/08/2019.
//  Copyright © 2019 nicolas.parimeros. All rights reserved.
//

import UIKit

class StatCategoryHeaderView: UIView {
    
    // MARK: Constants

    private let kMargin: CGFloat = 10

    // MARK: UI Components

    lazy var leftLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        return label
    }()
    
    lazy var rightLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        return label
    }()

    // MARK: Object lifecycle

    init() {
        super.init(frame: .zero)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: Setup UI

    private func setup() {
        backgroundColor = .white
        addSubview(leftLabel)
        addSubview(rightLabel)
        buildConstraints()
    }

    private func buildConstraints() {
        leftLabel.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(kMargin)
            make.left.equalToSuperview()
            make.bottom.equalToSuperview().offset(-kMargin)
            make.width.equalToSuperview().dividedBy(2)
        }
        rightLabel.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(kMargin)
            make.left.equalTo(leftLabel.snp.right)
            make.right.equalToSuperview()
            make.bottom.equalToSuperview().offset(-kMargin)
            make.width.equalToSuperview().dividedBy(2)
        }
    }

}
