//
//  PlayerStatCollectionViewCell.swift
//  NRLStats
//
//  Created by Nicolas Parimeros on 21/08/2019.
//  Copyright © 2019 nicolas.parimeros. All rights reserved.
//

import UIKit

class PlayerStatCollectionViewCell: UICollectionViewCell {
    
    // MARK: Constants

    private let kMargin: CGFloat = 10
    private let kImageSize: CGFloat = 60
    private let kCornerRadius: CGFloat = 8
    private let kBorderWidth: CGFloat = 1

    // MARK: UI Components

    public lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.masksToBounds = true
        imageView.backgroundColor = .lightGray
        return imageView
    }()

    public lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        return label
    }()

    public lazy var numberLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = UIFont.preferredFont(forTextStyle: .subheadline)
        return label
    }()

    public lazy var positionLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = UIFont.preferredFont(forTextStyle: .body)
        return label
    }()

    public lazy var statLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        return label
    }()

    // MARK: Object lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: Setup UI

    private func setup() {
        backgroundColor = .white

        layer.cornerRadius = kCornerRadius
        layer.borderWidth = kBorderWidth
        layer.borderColor = UIColor.lightGray.cgColor

        setupSubviews()
        setupConstraints()
    }

    private func setupSubviews() {
        addSubview(imageView)
        addSubview(nameLabel)
        addSubview(numberLabel)
        addSubview(positionLabel)
        addSubview(statLabel)
    }

    private func setupConstraints() {
        imageView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(kMargin)
            make.left.greaterThanOrEqualToSuperview()
            make.right.lessThanOrEqualToSuperview()
            make.centerX.equalToSuperview()
            make.size.equalTo(kImageSize)
        }
        nameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(imageView.snp.bottom).offset(kMargin)
            make.left.equalToSuperview().offset(kMargin)
            make.right.equalToSuperview().offset(-kMargin)
        }
        numberLabel.snp.makeConstraints { (make) in
            make.top.equalTo(nameLabel.snp.bottom).offset(kMargin)
            make.left.equalToSuperview().offset(kMargin)
            make.right.equalToSuperview().offset(-kMargin)
        }
        positionLabel.snp.makeConstraints { (make) in
            make.top.equalTo(numberLabel.snp.bottom).offset(kMargin)
            make.left.equalToSuperview().offset(kMargin)
            make.right.equalToSuperview().offset(-kMargin)
        }
        statLabel.snp.makeConstraints { (make) in
            make.top.equalTo(positionLabel.snp.bottom).offset(kMargin)
            make.left.equalToSuperview().offset(kMargin)
            make.right.equalToSuperview().offset(-kMargin)
            make.bottom.lessThanOrEqualToSuperview().offset(-kMargin)
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        imageView.layer.cornerRadius = imageView.bounds.width / 2
    }

}

// Extension to configure the cell easily with a StatCategoryPlayerCellViewModel

extension PlayerStatCollectionViewCell {

    func configure(viewModel: StatCategoryPlayerCellViewModel) {
        imageView.kf.setImage(with: viewModel.imageUrl) { [weak self] (result) in
            switch result {
            case .failure: self?.imageView.kf.setImage(with: URL(string: AppConstants.APIRessources.defaultImageUrl))
            default: break
            }
        }
        nameLabel.text = viewModel.shortName
        numberLabel.text = viewModel.jumperNumber
        positionLabel.text = viewModel.position
        statLabel.text = viewModel.stat
    }

}

