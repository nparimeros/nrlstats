//
//  AppConstants.swift
//  NRLStats
//
//  Created by Nicolas Parimeros on 20/08/2019.
//  Copyright © 2019 nicolas.parimeros. All rights reserved.
//

import Foundation

struct AppConstants {
    
    private init() { }
    
    struct General {
        private init() { }
        static let topPlayerStats = NSLocalizedString("top_player_stats", comment: "")
    }
    
    struct Localized {
        private init() {}
        static let headerStatsCategories = NSLocalizedString("header_stats_categories", comment: "")
        static let headerPlayerStatsGeneral = NSLocalizedString("header_player_stats_general", comment: "")
        static let headerPlayerStatsLastGame = NSLocalizedString("header_player_stats_last_game", comment: "")
        static let fullname = NSLocalizedString("fullname", comment: "")
        static let position = NSLocalizedString("position", comment: "")
    }
    
    struct APIRessources {
        private init() {}
        static let statsApiUrl =  "https://statsapi.foxsports.com.au/3.0/api/sports/league/matches/NRL20172101/topplayerstats.json;type=fantasy_points;type=tackles;type=runs;type=run_metres?limit=5&userkey=A00239D3-45F6-4A0A-810C-54A347F144C2"
        static let defaultImageUrl = "https://media.foxsports.com.au/match-centre/includes/images/headshots/headshot-blank-large.jpg"
        static let playerStatsApiUrlFormat = "https://statsapi.foxsports.com.au/3.0/api/sports/league/series/1/seasons/115/teams/%d/players/%d/detailedstats.json?&userkey=9024ec15-d791-4bfd-aa3b-5bcf5d36da4f"
        static let playerImageUrlFormat = "https://media.foxsports.com.au/match-centre/includes/images/headshots/landscape/nrl/%d.jpg"
    }
    
}
