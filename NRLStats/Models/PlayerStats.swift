//
//  PlayerStats.swift
//  NRLStats
//
//  Created by Nicolas Parimeros on 21/08/2019.
//  Copyright © 2019 nicolas.parimeros. All rights reserved.
//

import Foundation
import SwiftyJSON

// MARK: Model used to parse the player stats
// Using SwiftyJSON here to take advantage of their JSON type implementing Encodable
// To get the lastMatchStats as a Json property, and not get all the properties one by one
// The properties of the object will be displayed as key-value pairs
// Ideally each of these keys will match a localized key in our Localizable strings file.

struct PlayerStats: Decodable {

    let id: Int
    let surname: String
    let position: String
    let fullName: String
    let shortName: String
    let dateOfBirth: String
    let height: Int
    let otherNames: String
    let weight: Int
    let lastMatchId: String?
    let careerStats: JSON?
    let lastMatchStats: JSON?
    let seriesSeasonStats: JSON?

    enum CodingKeys: String, CodingKey {
        case id
        case surname
        case position
        case fullName = "full_name"
        case shortName = "short_name"
        case dateOfBirth = "date_of_birth"
        case height = "height_cm"
        case otherNames = "other_names"
        case weight = "weight_kg"
        case lastMatchId = "last_match_id"
        case careerStats = "career_stats"
        case lastMatchStats = "last_match_stats"
        case seriesSeasonStats = "series_season_stats"
    }
}
