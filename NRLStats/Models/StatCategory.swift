//
//  StatCategory.swift
//  NRLStats
//
//  Created by Nicolas Parimeros on 20/08/2019.
//  Copyright © 2019 nicolas.parimeros. All rights reserved.
//

import Foundation

// MARK: Model used to parse stats categories

struct StatCategory: Codable {
    let matchId: String
    let firstTeam: Team
    let secondTeam: Team
    let statType: String

    enum CodingKeys: String, CodingKey {
        case matchId = "match_id"
        case firstTeam = "team_A"
        case secondTeam = "team_B"
        case statType = "stat_type"
    }
}

// MARK: Extension to provide localized description of the stat type

extension StatCategory: CustomStringConvertible {
    
    var description: String {
        return NSLocalizedString(statType, comment: "")
    }
    
}
