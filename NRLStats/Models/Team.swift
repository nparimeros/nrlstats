//
//  Team.swift
//  NRLStats
//
//  Created by Nicolas Parimeros on 20/08/2019.
//  Copyright © 2019 nicolas.parimeros. All rights reserved.
//

import Foundation

// MARK: Model used to parse Team objects

struct Team: Codable {
    let id: Int
    let name: String
    let code: String
    let shortName: String
    let topPlayers: [Player]

    enum CodingKeys: String, CodingKey {
        case id
        case name
        case code
        case shortName = "short_name"
        case topPlayers = "top_players"
    }
}
