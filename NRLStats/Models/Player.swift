//
//  Player.swift
//  NRLStats
//
//  Created by Nicolas Parimeros on 20/08/2019.
//  Copyright © 2019 nicolas.parimeros. All rights reserved.
//

import Foundation

// MARK: Model used to parse Player objects

struct Player: Codable {
    let id: Int
    let position: String
    let fullName: String
    let shortName: String
    let statValue: Int
    let jumperNumber: Int

    enum CodingKeys: String, CodingKey {
        case id
        case position
        case fullName = "full_name"
        case shortName = "short_name"
        case statValue = "stat_value"
        case jumperNumber = "jumper_number"
    }
}

// MARK: Extension to get the player image url easily

extension Player {
    var imageUrl: URL? {
        return URL(string: String(format: AppConstants.APIRessources.playerImageUrlFormat, id))
    }
}
