//
//  PlayerDetailsStatsViewModel.swift
//  NRLStats
//
//  Created by Nicolas Parimeros on 21/08/2019.
//  Copyright © 2019 nicolas.parimeros. All rights reserved.
//

import Foundation
import SwiftyJSON

fileprivate typealias SectionItem = (title: String, value: String)

fileprivate struct PlayerDetailsStatsSectionViewModel {
    var header: String
    var content: [SectionItem]
}

protocol PlayerDetailsStatsViewModelProtocol {
    var apiService: PlayerStatsServiceProtocol? { get set }
    var reloadTableView: (() -> Void)? { get set }
    var title: String { get }
    var headerImageUrl: URL? { get }
    var numberOfSections: Int { get }
    func fetchPlayerStats()
    func title(for section: Int) -> String
    func numberOfRows(in section: Int) -> Int
    func cellViewModel(for indexPath: IndexPath) -> PlayerDetailsStatsCellViewModel
}

class PlayerDetailsStatsViewModel: PlayerDetailsStatsViewModelProtocol {

    // Mark: Constants
    private let kGeneralInformationsHeader = AppConstants.Localized.headerPlayerStatsGeneral
    private let kPlayerLastGameStatsHeader = AppConstants.Localized.headerPlayerStatsLastGame
    private let kNotAttributed = "N/A"

    // Mark: Dependencies

    var apiService: PlayerStatsServiceProtocol?

    // Mark: Helper properties

    private var playerStats: PlayerStats? {
        didSet {
            generateSections()
        }
    }

    private var sections: [PlayerDetailsStatsSectionViewModel] = [] {
        didSet {
            reloadTableView?()
        }
    }

    // Mark: Properties

    internal let player: Player
    internal let team: Team

    init(player: Player, team: Team) {
        self.player = player
        self.team = team
    }

    // Mark: UI binding

    var reloadTableView: (() -> Void)?

    var title: String {
        return player.shortName
    }

    var headerImageUrl: URL? {
        return player.imageUrl
    }

    // Mark: Data fetching

    func fetchPlayerStats() {
        apiService?.fetchPlayerStats(teamId: team.id, playerId: player.id, completion: { [weak self] (success, playerStats) in
            self?.playerStats = playerStats
        })
    }

    // Mark: Tableview binding

    func title(for section: Int) -> String {
        return sections[section].header
    }

    var numberOfSections: Int {
        return sections.count
    }

    func numberOfRows(in section: Int) -> Int {
        return sections[section].content.count
    }

    private func generateSections() {
        guard let playerStats = playerStats else { return }

        sections.removeAll()

        // Section 1 - General informations

        let generalSection = PlayerDetailsStatsSectionViewModel(header: kGeneralInformationsHeader, content: [
            (AppConstants.Localized.fullname, playerStats.fullName),
            (AppConstants.Localized.position, playerStats.position)
        ])
        sections.append(generalSection)

        // Section 2 - Last game stats
        let lastGameContent = playerStats.lastMatchStats?.map { (json) -> SectionItem in
            let value = json.1
            let stringValue = value.stringValue.isEmpty ? kNotAttributed : value.stringValue
            // Using a small hack to make the keys more readable
            // Ideally we'd make the keys match localized keys in Localizable string file
            let readableKey = json.0.readableJsonKey
            return SectionItem(readableKey, stringValue)
        }
        
        if let lastGameContent = lastGameContent {
            let lastGameSection = PlayerDetailsStatsSectionViewModel(header: kPlayerLastGameStatsHeader, content: lastGameContent)
            sections.append(lastGameSection)
        }

    }

    func cellViewModel(for indexPath: IndexPath) -> PlayerDetailsStatsCellViewModel {
        let content = sections[indexPath.section].content[indexPath.row]
        return PlayerDetailsStatsCellViewModel(title: content.title, detail: content.value)
    }

}

fileprivate extension String {

    var readableJsonKey: String {
        return self.replacingOccurrences(of: "_", with: " ").capitalized.trimmingCharacters(in: .whitespaces)
    }

}

