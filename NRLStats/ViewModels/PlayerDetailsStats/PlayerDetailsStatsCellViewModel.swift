//
//  PlayerDetailsStatsCellViewModel.swift
//  NRLStats
//
//  Created by Nicolas Parimeros on 21/08/2019.
//  Copyright © 2019 nicolas.parimeros. All rights reserved.
//

import Foundation

struct PlayerDetailsStatsCellViewModel {
    var title: String
    var detail: String
}
