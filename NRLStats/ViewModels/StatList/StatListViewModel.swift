//
//  StatListViewModel.swift
//  NRLStats
//
//  Created by Nicolas Parimeros on 21/08/2019.
//  Copyright © 2019 nicolas.parimeros. All rights reserved.
//

import Foundation

protocol StatListViewModelProtocol {

    var apiService: APIStatsServiceProtocol? { get set }

    var reloadTableView: (() -> Void)? { get set }

    var updateLoadingStatus: (() -> Void)? { get set }

    var isLoading: Bool { get set }

    var numberOfSections: Int { get }

    var numberOfCells: Int { get }

    func fetchStatCategories()

    func title(for section: Int) -> String?

    func cellViewModel(for indexPath: IndexPath) -> StatCategoryCellViewModel

    func statCategoryViewModel(for indexPath: IndexPath) -> StatCategoryViewModel
}

class StatListViewModel: StatListViewModelProtocol {

    // Mark: Dependencies

    var apiService: APIStatsServiceProtocol?

    // Mark: Properties

    private var cellViewModels: [StatCategoryCellViewModel] = [] {
        didSet {
            self.reloadTableView?()
        }
    }

    private var statCategories: [StatCategory] = [] {
        didSet {
            cellViewModels = statCategories.map({ StatCategoryCellViewModel(category: $0) })
        }
    }

    // Mark: UI binding

    var reloadTableView: (() -> Void)?

    var updateLoadingStatus: (() -> Void)?

    var isLoading = false {
        didSet {
            self.updateLoadingStatus?()
        }
    }

    // Mark: Data fetching

    func fetchStatCategories() {

        isLoading = true

        apiService?.fetchStatCategories(completion: { [weak self] (success, categories) in

            self?.isLoading = false

            guard success, let categories = categories else { return }

            self?.statCategories = categories

        })
    }

    // Mark: Tableview binding

    func title(for section: Int) -> String? {
        guard !statCategories.isEmpty else { return nil }
        return AppConstants.Localized.headerStatsCategories
    }

    var numberOfSections: Int {
        return 1
    }

    var numberOfCells: Int {
        return cellViewModels.count
    }

    func cellViewModel(for indexPath: IndexPath) -> StatCategoryCellViewModel {
        return cellViewModels[indexPath.row]
    }

    // Mark: Data passing

    func statCategoryViewModel(for indexPath: IndexPath) -> StatCategoryViewModel {
        let category = statCategories[indexPath.row]
        return StatCategoryViewModel(category: category)
    }
}
