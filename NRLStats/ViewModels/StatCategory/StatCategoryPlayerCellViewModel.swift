//
//  StatCategoryPlayerCellViewModel.swift
//  NRLStats
//
//  Created by Nicolas Parimeros on 21/08/2019.
//  Copyright © 2019 nicolas.parimeros. All rights reserved.
//

import Foundation

struct StatCategoryPlayerCellViewModel {
    var shortName: String
    var jumperNumber: String
    var position: String
    var stat: String
    var imageUrl: URL?
    
    init(player: Player) {
        self.shortName = player.shortName
        self.jumperNumber = "#\(player.jumperNumber)"
        self.position = player.position
        self.stat = "\(player.statValue)"
        self.imageUrl = player.imageUrl
    }
}
