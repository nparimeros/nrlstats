//
//  StatCategoryViewModel.swift
//  NRLStats
//
//  Created by Nicolas Parimeros on 20/08/2019.
//  Copyright © 2019 nicolas.parimeros. All rights reserved.
//

import Foundation

fileprivate struct PlayerTupleSectionViewModel {
    var playerTeamA: StatCategoryPlayerCellViewModel
    var playerTeamB: StatCategoryPlayerCellViewModel
}

protocol StatCategoryViewModelProtocol {

    var reloadCollectionView: (() -> Void)? { get set }

    var title: String { get }

    var leftHeaderText: String { get }

    var rightHeaderText: String { get }

    var numberOfSections: Int { get }

    var numberOfItemsPerSection: Int { get }

    func fetchTeamStats()

    func cellViewModel(for indexPath: IndexPath) -> StatCategoryPlayerCellViewModel

    func playerStatsViewModel(for indexPath: IndexPath) -> PlayerDetailsStatsViewModel
}

class StatCategoryViewModel: StatCategoryViewModelProtocol {

    // Mark: Properties

    private var sections: [PlayerTupleSectionViewModel] = [] {
        didSet {
            reloadCollectionView?()
        }
    }

    private var category: StatCategory

    init(category: StatCategory) {
        self.category = category
    }

    // Mark: Fetching data

    func fetchTeamStats() {
        // Iterate through the first team players just in case the number of players displayed is the same for both teams
        var sections: [PlayerTupleSectionViewModel] = []
        for (index, value) in category.firstTeam.topPlayers.enumerated() {
            guard index < category.secondTeam.topPlayers.count else { break }
            let playerA = StatCategoryPlayerCellViewModel(player: value)
            let playerB = StatCategoryPlayerCellViewModel(player: category.secondTeam.topPlayers[index])
            sections.append(PlayerTupleSectionViewModel(playerTeamA: playerA, playerTeamB: playerB))
        }
        self.sections = sections
    }

    // Mark: UI binding

    var reloadCollectionView: (() -> Void)?

    var title: String {
        return category.description
    }

    var leftHeaderText: String {
        return category.firstTeam.name
    }

    var rightHeaderText: String {
        return category.secondTeam.name
    }

    // Mark: CollectionView binding

    var numberOfSections: Int {
        return sections.count
    }

    var numberOfItemsPerSection: Int {
        return 2
    }

    func cellViewModel(for indexPath: IndexPath) -> StatCategoryPlayerCellViewModel {
        let section = sections[indexPath.section]
        return indexPath.item == 0 ? section.playerTeamA : section.playerTeamB
    }

    // Mark: Data passing

    func playerStatsViewModel(for indexPath: IndexPath) -> PlayerDetailsStatsViewModel {
        let selectedTeam = team(for: indexPath)
        let selectedPlayer = player(for: selectedTeam, indexPath: indexPath)
        let viewModel = PlayerDetailsStatsViewModel(player: selectedPlayer, team: selectedTeam)
        viewModel.apiService = PlayerStatsService()
        return viewModel
    }

    // MARK: Helpers

    private func player(for team: Team, indexPath: IndexPath) -> Player {
        return team.topPlayers[indexPath.section]
    }

    private func team(for indexPath: IndexPath) -> Team {
        return indexPath.item == 0 ? category.firstTeam : category.secondTeam
    }

}
