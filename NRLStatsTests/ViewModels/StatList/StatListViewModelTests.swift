//
//  StatListViewModelTests.swift
//  NRLStatsTests
//
//  Created by Nicolas Parimeros on 22/08/2019.
//  Copyright © 2019 nicolas.parimeros. All rights reserved.
//

@testable import NRLStats
import XCTest

class StatListViewModelTests: XCTestCase {
    
    // MARK: - Subject under test
    
    var sut: StatListViewModel!
    
    // MARK: - Test lifecycle
    
    override func setUp() {
        super.setUp()
        setupStatListViewModel()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: - Test setup
    
    func setupStatListViewModel() {
        sut = StatListViewModel()
    }
    
    // MARK: - Test doubles
    
    class APIStatsServiceSpy: APIStatsServiceProtocol {
        
        var fetchStatCategoriesCalled = false
        
        func fetchStatCategories(completion: @escaping (Bool, [StatCategory]?) -> Void) {
            fetchStatCategoriesCalled = true
        }
        
    }
    
    class APIStatsServiceSuccess: APIStatsServiceProtocol {
        
        var category: StatCategory
        
        init() {
            let team = Team(id: 0, name: "", code: "", shortName: "", topPlayers: [])
            category = StatCategory(matchId: "1", firstTeam: team, secondTeam: team, statType: "")
        }
        
        func fetchStatCategories(completion: @escaping (Bool, [StatCategory]?) -> Void) {
            return completion(true, [category])
        }
        
    }
    
    class APIStatsServiceFailure: APIStatsServiceProtocol {
        
        func fetchStatCategories(completion: @escaping (Bool, [StatCategory]?) -> Void) {
            return completion(false, nil)
        }
        
    }
    
    // MARK: - Tests
    
    func testFetchCategoriesShouldAskApiServiceToFetchCategories() {
        
        // Given
        let spy = APIStatsServiceSpy()
        sut.apiService = spy
        
        // When
        sut.fetchStatCategories()
        
        // Then
        XCTAssert(spy.fetchStatCategoriesCalled)
    }
    
    func testFetchCategoriesApiSuccess() {
        
        // Given
        let stub = APIStatsServiceSuccess()
        sut.apiService = stub
        
        // When
        sut.fetchStatCategories()
        
        // Then
        XCTAssertEqual(sut.numberOfCells, 1)
    }
    
    func testFetchCategoriesApiFailure() {
        
        // Given
        let stub = APIStatsServiceFailure()
        sut.apiService = stub
        
        // When
        sut.fetchStatCategories()
        
        // Then
        XCTAssertEqual(sut.numberOfCells, 0)
    }
    
    func testTitleForSectionIfEmpty() {
        
        // Given
        let stub = APIStatsServiceFailure()
        sut.apiService = stub
        
        // When
        sut.fetchStatCategories()
        
        // Then
        XCTAssertNil(sut.title(for: 0))
    }
    
    func testTitleForSectionIfNotEmpty() {
        
        // Given
        let stub = APIStatsServiceSuccess()
        sut.apiService = stub
        
        // When
        sut.fetchStatCategories()
        
        // Then
        XCTAssertNotNil(sut.title(for: 0))
    }
    
    func testNumberOfCellsIfEmpty() {
        
        // Given
        let stub = APIStatsServiceFailure()
        sut.apiService = stub
        
        // When
        sut.fetchStatCategories()
        
        // Then
        XCTAssertEqual(sut.numberOfCells, 0)
    }
    
    func testNumberOfCellsIfNotEmpty() {
        
        // Given
        let stub = APIStatsServiceSuccess()
        sut.apiService = stub
        
        // When
        sut.fetchStatCategories()
        
        // Then
        XCTAssertEqual(sut.numberOfCells, 1)
    }
    
    func testCellViewModelForIndexPath() {
        
        // Given
        let stub = APIStatsServiceSuccess()
        sut.apiService = stub
        
        // When
        sut.fetchStatCategories()
        
        // Then
        let vm = StatCategoryCellViewModel(category: stub.category)
        XCTAssertEqual(sut.cellViewModel(for: IndexPath(row: 0, section: 0)).title, vm.title)
    }
    
    func testStatCategoryViewModelForIndexPath() {
        
        // Given
        let stub = APIStatsServiceSuccess()
        sut.apiService = stub
        
        // When
        sut.fetchStatCategories()
        
        // Then
        XCTAssertEqual(sut.statCategoryViewModel(for: IndexPath(row: 0, section: 0)).title, stub.category.statType)
    }
    
}
