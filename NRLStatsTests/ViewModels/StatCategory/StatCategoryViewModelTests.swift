//
//  StatCategoryViewModelTests.swift
//  NRLStatsTests
//
//  Created by Nicolas Parimeros on 22/08/2019.
//  Copyright © 2019 nicolas.parimeros. All rights reserved.
//

import XCTest
@testable import NRLStats

class StatCategoryViewModelTests: XCTestCase {
    
    // MARK: - Subject under test
    
    var sut: StatCategoryViewModel!
    var testPlayer: Player!
    
    // MARK: - Test lifecycle
    
    override func setUp() {
        super.setUp()
        setupStatCategoryViewModel()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: - Test setup
    
    func setupStatCategoryViewModel() {
        testPlayer = Player(id: 1, position: "top", fullName: "John Doe", shortName: "J.Doe", statValue: 10, jumperNumber: 10)
        let team1 = Team(id: 1, name: "Team 1", code: "Team 1", shortName: "Team 1", topPlayers: [testPlayer])
        let team2 = Team(id: 2, name: "Team 2", code: "Team 2", shortName: "Team 2", topPlayers: [testPlayer])
        let category = StatCategory(matchId: "1", firstTeam: team1, secondTeam: team2, statType: "test")
        sut = StatCategoryViewModel(category: category)
    }
    
    // MARK: - Tests
    
    func testTitle() {
        
        // Given
        
        // When
        
        // Then
        XCTAssertEqual(sut.title, "test")
    }
    
    func testLeftHeaderText() {
        
        // Given
        
        // When
        
        
        // Then
        XCTAssertEqual(sut.leftHeaderText, "Team 1")
    }
    
    func testRightHeaderText() {
        
        // Given
        
        // When
        
        
        // Then
        XCTAssertEqual(sut.rightHeaderText, "Team 2")
    }
    
    func testNumberOfSections() {
        
        // Given
        
        // When
        sut.fetchTeamStats()
        
        // Then
        XCTAssertEqual(sut.numberOfSections, 1)
    }
    
    func testNumberOfItemsPerSection() {
        
        // Given
        
        // When
        sut.fetchTeamStats()
        
        // Then
        XCTAssertEqual(sut.numberOfItemsPerSection, 2)
    }
    
    func testPlayerStatsViewModelForIndexPath() {
        
        // Given
        
        // When
        sut.fetchTeamStats()
        
        // Then
        let vm = sut.playerStatsViewModel(for: IndexPath(item: 0, section: 0))
        XCTAssertEqual(vm.team.id, 1)
        XCTAssertEqual(vm.player.id, 1)
        XCTAssertNotNil(vm.apiService)
    }
    
    func testCellViewModelForIndexPath() {
        
        // Given
        
        // When
        sut.fetchTeamStats()
        
        // Then
        let vm = sut.cellViewModel(for: IndexPath(item: 0, section: 0))
        XCTAssertEqual(vm.shortName, testPlayer.shortName)
        XCTAssertEqual(vm.imageUrl, testPlayer.imageUrl)
        XCTAssertEqual(vm.jumperNumber, "#\(testPlayer.jumperNumber)")
        XCTAssertEqual(vm.stat, "\(testPlayer.statValue)")
        XCTAssertEqual(vm.position, testPlayer.position)
    }
    
}
