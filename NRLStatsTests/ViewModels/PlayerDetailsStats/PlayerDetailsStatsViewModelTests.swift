//
//  PlayerDetailsStatsViewModelTests.swift
//  NRLStatsTests
//
//  Created by Nicolas Parimeros on 22/08/2019.
//  Copyright © 2019 nicolas.parimeros. All rights reserved.
//

import XCTest
@testable import NRLStats

class PlayerDetailsStatsViewModelTests: XCTestCase {
    // MARK: - Subject under test

    var sut: PlayerDetailsStatsViewModel!
    var testPlayer: Player!

    // MARK: - Test lifecycle

    override func setUp() {
        super.setUp()
        setupPlayerDetailsStatsViewModel()
    }

    override func tearDown() {
        super.tearDown()
    }

    // MARK: - Test setup

    func setupPlayerDetailsStatsViewModel() {
        testPlayer = Player(id: 0, position: "", fullName: "", shortName: "", statValue: 0, jumperNumber: 0)
        let team = Team(id: 0, name: "", code: "", shortName: "", topPlayers: [])
        sut = PlayerDetailsStatsViewModel(player: testPlayer, team: team)
    }

    // MARK: - Test doubles

    class APIPlayerStatsServiceSpy: PlayerStatsServiceProtocol {

        var fetchPlayerStatsCalled = false

        func fetchPlayerStats(teamId: Int, playerId: Int, completion: @escaping (Bool, PlayerStats?) -> Void) {
            fetchPlayerStatsCalled = true
        }

    }

    class APIPlayerStatsServiceStubSuccess: PlayerStatsServiceProtocol {
        
        let fullName = "John Doe"
        let position = "top"
        
        func fetchPlayerStats(teamId: Int, playerId: Int, completion: @escaping (Bool, PlayerStats?) -> Void) {
            let stats = PlayerStats(id: 0, surname: "Doe", position: position, fullName: fullName, shortName: "J.Doe", dateOfBirth: "", height: 0, otherNames: "", weight: 0, lastMatchId: nil, careerStats: nil, lastMatchStats: nil, seriesSeasonStats: nil)
            return completion(true, stats)
        }

    }

    class APIPlayerStatsServiceStubFailure: APIStatsServiceProtocol {

        func fetchStatCategories(completion: @escaping (Bool, [StatCategory]?) -> Void) {
            return completion(false, nil)
        }

    }

    // MARK: - Tests

    func testTitle() {
        // Given

        // When

        // Then
        XCTAssertEqual(sut.title, testPlayer.shortName)
    }

    func testHeaderImageUrl() {
        // Given

        // When

        // Then
        XCTAssertEqual(sut.headerImageUrl, testPlayer.imageUrl)
    }

    func testFetchPlayerStatsShouldCallApiService() {

        // Given
        let spy = APIPlayerStatsServiceSpy()
        sut.apiService = spy

        // When
        sut.fetchPlayerStats()

        // Then
        XCTAssert(spy.fetchPlayerStatsCalled)
    }

    func testTitleForSection() {

        // Given
        let stub = APIPlayerStatsServiceStubSuccess()
        sut.apiService = stub

        // When
        sut.fetchPlayerStats()

        // Then
        XCTAssertNotNil(sut.title(for: 0))
    }

    func testNumberOfSections() {

        // Given
        let stub = APIPlayerStatsServiceStubSuccess()
        sut.apiService = stub

        // When
        sut.fetchPlayerStats()

        // Then
        XCTAssertEqual(sut.numberOfSections, 1)
    }

    func testNumberOfRowsInSection() {

        // Given
        let stub = APIPlayerStatsServiceStubSuccess()
        sut.apiService = stub

        // When
        sut.fetchPlayerStats()

        // Then
        XCTAssertEqual(sut.numberOfRows(in: 0), 2)
    }

    func testCellViewModelForIndexPath() {

        // Given
        let stub = APIPlayerStatsServiceStubSuccess()
        sut.apiService = stub

        // When
        sut.fetchPlayerStats()

        // Then
        let vm1 = sut.cellViewModel(for: IndexPath(row: 0, section: 0))
        let vm2 = sut.cellViewModel(for: IndexPath(row: 1, section: 0))
        XCTAssertEqual(vm1.detail, stub.fullName)
        XCTAssertEqual(vm2.detail, stub.position)
    }

}
