//
//  PlayerDetailsStatsViewControllerTests.swift
//  NRLStatsTests
//
//  Created by Nicolas Parimeros on 22/08/2019.
//  Copyright © 2019 nicolas.parimeros. All rights reserved.
//

import XCTest
@testable import NRLStats

class PlayerDetailsStatsViewControllerTests: XCTestCase {

    private let kCellIdentifier = "stat-cell"

    // MARK: - Subject under test

    var sut: PlayerDetailsStatsViewController!

    // MARK: - Test lifecycle

    override func setUp() {
        super.setUp()
        setupPlayerDetailsStatsViewController()
    }

    override func tearDown() {
        super.tearDown()
    }

    // MARK: - Test setup

    func setupPlayerDetailsStatsViewController() {
        let player = Player(id: 0, position: "", fullName: "", shortName: "", statValue: 0, jumperNumber: 0)
        let team = Team(id: 0, name: "", code: "", shortName: "", topPlayers: [])
        let vm = PlayerDetailsStatsViewModel(player: player, team: team)
        sut = PlayerDetailsStatsViewController(viewModel: vm)
    }

    // MARK: - Test doubles

    class PlayerStatsViewModelSpy: PlayerDetailsStatsViewModelProtocol {
        
        var fetchPlayerStatsCalled = false
        var titleForSectionCalled = false
        var numberOfRowsInSectionCalled = false
        var cellViewModelForIndexPathCalled = false
        
        var apiService: PlayerStatsServiceProtocol?
        
        var reloadTableView: (() -> Void)?
        
        var title: String = ""
        
        var headerImageUrl: URL?
        
        var numberOfSections: Int = 0
        
        func fetchPlayerStats() {
            fetchPlayerStatsCalled = true
        }
        
        func title(for section: Int) -> String {
            titleForSectionCalled = true
            return ""
        }
        
        func numberOfRows(in section: Int) -> Int {
            numberOfRowsInSectionCalled = true
            return 1
        }
        
        func cellViewModel(for indexPath: IndexPath) -> PlayerDetailsStatsCellViewModel {
            cellViewModelForIndexPathCalled = true
            return PlayerDetailsStatsCellViewModel(title: "", detail: "")
        }
        
    }

    class PlayerStatsViewModelStub: PlayerDetailsStatsViewModelProtocol {
        
        var apiService: PlayerStatsServiceProtocol?
        
        var reloadTableView: (() -> Void)?
        
        var title: String = ""
        
        var headerImageUrl: URL?
        
        var numberOfSections: Int = 1
        
        func fetchPlayerStats() {
            
        }
        
        func title(for section: Int) -> String {
            return "test"
        }
        
        func numberOfRows(in section: Int) -> Int {
            return 1
        }
        
        func cellViewModel(for indexPath: IndexPath) -> PlayerDetailsStatsCellViewModel {
            return PlayerDetailsStatsCellViewModel(title: "title", detail: "detail")
        }
    }

    // MARK: - Tests

    func testViewDidLoad() {
        // Given
        let spy = PlayerStatsViewModelSpy()
        sut.viewModel = spy

        // When
        sut.viewDidLoad()

        // Then
        XCTAssertNotNil(spy.reloadTableView)
        XCTAssert(spy.fetchPlayerStatsCalled)
    }

    func testNumberOfSections() {

        // Given
        let stub = PlayerStatsViewModelStub()
        sut.viewModel = stub

        // When
        let number = sut.numberOfSections(in: UITableView())

        // Then
        XCTAssertEqual(stub.numberOfSections, number)
    }
    
    func testTitleForHeaderInSection() {
        // Given
        let spy = PlayerStatsViewModelSpy()
        sut.viewModel = spy
        
        // When
        let _ = sut.tableView(UITableView(), titleForHeaderInSection: 0)
        
        // Then
        XCTAssert(spy.titleForSectionCalled)
    }

    func testNumberOfRowsInSection() {

        // Given
        let spy = PlayerStatsViewModelSpy()
        sut.viewModel = spy

        // When
        let _ = sut.tableView(UITableView(), numberOfRowsInSection: 0)

        // Then
        XCTAssert(spy.numberOfRowsInSectionCalled)
    }
    
    func testCellForRowAtIndexPath() {
        
        // Given
        let spy = PlayerStatsViewModelSpy()
        sut.viewModel = spy
        
        // When
        let _ = sut.tableView(UITableView(), cellForRowAt: IndexPath(row: 0, section: 0))
        
        // Then
        XCTAssert(spy.cellViewModelForIndexPathCalled)
    }

    func testCellContent() {

        // Given
        let stub = PlayerStatsViewModelStub()
        sut.viewModel = stub

        // When
        let cell = sut.tableView(UITableView(), cellForRowAt: IndexPath(row: 0, section: 0))

        // Then
        XCTAssertEqual(cell.textLabel?.text, "title")
        XCTAssertEqual(cell.detailTextLabel?.text, "detail")
    }


}
