//
//  StatCategoryViewControllerTests.swift
//  NRLStatsTests
//
//  Created by Nicolas Parimeros on 22/08/2019.
//  Copyright © 2019 nicolas.parimeros. All rights reserved.
//

@testable import NRLStats
import XCTest

class StatCategoryViewControllerTests: XCTestCase {
    
    private let kCellIdentifier = "stat-cell"
    
    // MARK: - Subject under test
    
    var sut: StatCategoryViewController!
    
    var collectionView: UICollectionView!
    
    // MARK: - Test lifecycle
    
    override func setUp() {
        super.setUp()
        setupStatCategoryViewController()
        setupCollectionView()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: - Test setup
    
    func setupStatCategoryViewController() {
        sut = StatCategoryViewController()
    }
    
    func setupCollectionView() {
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        collectionView.register(PlayerStatCollectionViewCell.self, forCellWithReuseIdentifier: kCellIdentifier)
    }
    
    // MARK: - Test doubles
    
    class StatCategoryViewModelSpy: StatCategoryViewModelProtocol {
        
        var fetchTeamStatsCalled = false
        
        var reloadCollectionView: (() -> Void)?
        
        var title: String = ""
        
        var leftHeaderText: String = ""
        
        var rightHeaderText: String = ""
        
        var numberOfSections: Int = 1
        
        var numberOfItemsPerSection: Int = 1
        
        func fetchTeamStats() {
            fetchTeamStatsCalled = true
        }
        
        func cellViewModel(for indexPath: IndexPath) -> StatCategoryPlayerCellViewModel {
            let player = Player(id: 0, position: "", fullName: "", shortName: "", statValue: 0, jumperNumber: 0)
            return StatCategoryPlayerCellViewModel(player: player)
        }
        
        func playerStatsViewModel(for indexPath: IndexPath) -> PlayerDetailsStatsViewModel {
            let player = Player(id: 0, position: "", fullName: "", shortName: "", statValue: 0, jumperNumber: 0)
            let team = Team(id: 0, name: "", code: "", shortName: "", topPlayers: [])
            return PlayerDetailsStatsViewModel(player: player, team: team)
        }
        
    }
    
    class StatCategoryViewModelStub: StatCategoryViewModelProtocol {
        
        var reloadCollectionView: (() -> Void)?
        
        var title: String = ""
        
        var leftHeaderText: String = ""
        
        var rightHeaderText: String = ""
        
        var numberOfSections: Int = 1
        
        var numberOfItemsPerSection: Int = 1
        
        func fetchTeamStats() {
            
        }
        
        func cellViewModel(for indexPath: IndexPath) -> StatCategoryPlayerCellViewModel {
            let player = Player(id: 0, position: "", fullName: "", shortName: "", statValue: 0, jumperNumber: 0)
            return StatCategoryPlayerCellViewModel(player: player)
        }
        
        func playerStatsViewModel(for indexPath: IndexPath) -> PlayerDetailsStatsViewModel {
            let player = Player(id: 0, position: "", fullName: "", shortName: "", statValue: 0, jumperNumber: 0)
            let team = Team(id: 0, name: "", code: "", shortName: "", topPlayers: [])
            return PlayerDetailsStatsViewModel(player: player, team: team)
        }
        
    }
    
    // MARK: - Tests
    
    func testViewDidLoad() {
        // Given
        let spy = StatCategoryViewModelSpy()
        sut.viewModel = spy
        
        // When
        sut.viewDidLoad()
        
        // Then
        XCTAssertNotNil(spy.reloadCollectionView)
        XCTAssert(spy.fetchTeamStatsCalled)
    }
    
    func testNumberOfSections() {
        
        // Given
        let stub = StatCategoryViewModelStub()
        sut.viewModel = stub
        
        // When
        let number = sut.numberOfSections(in: collectionView)
        
        // Then
        XCTAssertEqual(number, stub.numberOfSections)
    }
    
    func testNumberOfItemsInSection() {
        
        // Given
        let stub = StatCategoryViewModelStub()
        sut.viewModel = stub
        
        // When
        let number = sut.collectionView(collectionView, numberOfItemsInSection: 0)
        
        // Then
        XCTAssertEqual(number, stub.numberOfItemsPerSection)
    }
    
    func testCellContent() {
        
        // Given
        let stub = StatCategoryViewModelStub()
        sut.viewModel = stub
        
        // When
        let cell = sut.collectionView(collectionView, cellForItemAt: IndexPath(item: 0, section: 0)) as! PlayerStatCollectionViewCell
        
        // Then
        XCTAssertEqual(cell.nameLabel.text, stub.cellViewModel(for: IndexPath(item: 0, section: 0)).shortName)
    }
    
}
