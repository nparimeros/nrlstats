//
//  StatListViewControllerTests.swift
//  NRLStatsTests
//
//  Created by Nicolas Parimeros on 22/08/2019.
//  Copyright © 2019 nicolas.parimeros. All rights reserved.
//

@testable import NRLStats
import XCTest

class StatListViewControllerTests: XCTestCase {
    
    // MARK: - Subject under test
    
    var sut: StatListViewController!
    
    // MARK: - Test lifecycle
    
    override func setUp() {
        super.setUp()
        setupStatListViewController()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: - Test setup
    
    func setupStatListViewController() {
        sut = StatListViewController()
    }
    
    // MARK: - Test doubles
    
    class StatListViewModelSpy: StatListViewModelProtocol {
        
        var fetchStatCategoriesCalled = false
        
        var apiService: APIStatsServiceProtocol?
        
        var reloadTableView: (() -> Void)?
        
        var updateLoadingStatus: (() -> Void)?
        
        var isLoading: Bool = false
        
        var numberOfSections: Int = 0
        
        var numberOfCells: Int = 0
        
        func fetchStatCategories() {
            fetchStatCategoriesCalled = true
        }
        
        func title(for section: Int) -> String? {
            return nil
        }
        
        func cellViewModel(for indexPath: IndexPath) -> StatCategoryCellViewModel {
            return StatCategoryCellViewModel(title: "")
        }
        
        func statCategoryViewModel(for indexPath: IndexPath) -> StatCategoryViewModel {
            let team = Team(id: 0, name: "", code: "", shortName: "", topPlayers: [])
            let category = StatCategory(matchId: "1", firstTeam: team, secondTeam: team, statType: "")
            return StatCategoryViewModel(category: category)
        }
        
    }
    
    class StatListViewModelStub: StatListViewModelProtocol {
        
        var fetchStatCategoriesCalled = false
        
        var apiService: APIStatsServiceProtocol?
        
        var reloadTableView: (() -> Void)?
        
        var updateLoadingStatus: (() -> Void)?
        
        var isLoading: Bool = false
        
        var numberOfSections: Int = 1
        
        var numberOfCells: Int = 1
        
        func fetchStatCategories() {
            fetchStatCategoriesCalled = true
        }
        
        func title(for section: Int) -> String? {
            return "test"
        }
        
        func cellViewModel(for indexPath: IndexPath) -> StatCategoryCellViewModel {
            return StatCategoryCellViewModel(title: "test")
        }
        
        func statCategoryViewModel(for indexPath: IndexPath) -> StatCategoryViewModel {
            let team = Team(id: 0, name: "", code: "", shortName: "", topPlayers: [])
            let category = StatCategory(matchId: "1", firstTeam: team, secondTeam: team, statType: "test")
            return StatCategoryViewModel(category: category)
        }
        
    }
    
    // MARK: - Tests
    
    func testViewDidLoadShouldFetchStatCategories() {
        
        // Given
        let spy = StatListViewModelSpy()
        sut.viewModel = spy
        
        // When
        sut.viewDidLoad()
        
        // Then
        XCTAssertNotNil(spy.updateLoadingStatus)
        XCTAssertNotNil(spy.reloadTableView)
        XCTAssert(spy.fetchStatCategoriesCalled)
    }
    
    func testTitleForHeaderInSection() {
        
        // Given
        let stub = StatListViewModelStub()
        sut.viewModel = stub
        
        // When
        let title = sut.tableView(UITableView(), titleForHeaderInSection: 0)
        
        // Then
        XCTAssertEqual(title, stub.title(for: 0))
    }
    
    func testNumberOfSections() {
        
        // Given
        let stub = StatListViewModelStub()
        sut.viewModel = stub
        
        // When
        let number = sut.numberOfSections(in: UITableView())
        
        // Then
        XCTAssertEqual(number, stub.numberOfSections)
    }
    
    func testNumberOfRowsInSection() {
        
        // Given
        let stub = StatListViewModelStub()
        sut.viewModel = stub
        
        // When
        let number = sut.tableView(UITableView(), numberOfRowsInSection: 0)
        
        // Then
        XCTAssertEqual(number, stub.numberOfCells)
    }
    
    func testCellContent() {
        
        // Given
        let stub = StatListViewModelStub()
        sut.viewModel = stub
        
        // When
        let cell = sut.tableView(UITableView(), cellForRowAt: IndexPath(row: 0, section: 0))
        
        // Then
        XCTAssertEqual(cell.textLabel?.text, "test")
    }
    
}
